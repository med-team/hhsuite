Bug-Database: https://github.com/soedinglab/hh-suite/issues
Bug-Submit: https://github.com/soedinglab/hh-suite/issues/new
Reference:
 - Author: >
    Michael Remmert and Andreas Biegert and Andreas Hauser and Johannes Söding
   Title: >
    HHblits: Lightning-fast iterative protein sequence searching
    by HMM-HMM alignment.
   Journal: Nat. Methods
   DOI: 10.1038/NMETH.1818
   Year: 2011
   Volume: 9
   Number: 2
   Pages: 173-175
   URL: http://www.nature.com/nmeth/journal/v9/n2/full/nmeth.1818.html
   PMID: 22198341
 - Author: Johannes Söding
   Title: Protein homology detection by HMM-HMM comparison.
   Journal: Bioinformatics
   Volume: 21
   Number: 7
   Pages: 951-960
   Year: 2005
   PMID: 15531603
   DOI: 10.1093/bioinformatics/bti125
   URL: http://bioinformatics.oxfordjournals.org/content/21/7/951.abstract
   eprint: "http://bioinformatics.oxfordjournals.org/content/\
    21/7/951.full.pdf+html"
 - Author: >
     Martin Steinegger and Markus Meier and Milot Mirdita and Harald Voehringer
     and Stephan J. Haunsberger and Johannes Söding
   Title: >
     HH-suite3 for fast remote homology detection and deep protein
     annotation
   Journal: bioRxiv
   publisher: Cold Spring Harbor Laboratory
   Year: 2019
   DOI: 10.1101/560029
   URL: https://www.biorxiv.org/content/early/2019/02/25/560029
   eprint: https://www.biorxiv.org/content/early/2019/02/25/560029.full.pdf
Registry:
 - Name: OMICtools
   Entry: OMICS_28407
 - Name: bio.tools
   Entry: NA
 - Name: SciCrunch
   Entry: SCR_010277
 - Name: conda:bioconda
   Entry: hhsuite
Repository: https://github.com/soedinglab/hh-suite.git
Repository-Browse: https://github.com/soedinglab/hh-suite
